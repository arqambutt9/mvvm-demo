//
//  SellDataBaseManager.swift
//  TestDemo
//
//  Created by Arqam Butt on 01/08/2022.
//

import Foundation

class SellDataBaseManager {
    
    private let _sellDataBaseReposatory = SellDataBaseReposatory()
    
    func create(json: [[String:Any]]) {
        _sellDataBaseReposatory.create(json: json)
    }
    
    func getAll() -> [ItemToSell]? {
        return _sellDataBaseReposatory.getAll()
    }
    
    func delete(all record: [ItemToSell]) -> [ItemToSell]? {
        return _sellDataBaseReposatory.delete(all: record)
    }
    

    
}
