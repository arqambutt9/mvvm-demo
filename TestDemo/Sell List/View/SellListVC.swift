//
//  SellListVC.swift
//  Test 1
//
//  Created by Arqam Butt on 31/07/2022.
//

import UIKit

class SellListVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var sellViewModel = SellViewModel()
    
    deinit {
        print("Sell De init")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(SellCell.nib(), forCellReuseIdentifier: SellCell.cellIdentifier)
        sellViewModel.setDataFromDatabase()
        bindData()
    }

    private func bindData() {
        sellViewModel.data.bind { [weak self] _ in
            self?.tableView.reloadData()
        }
        
        sellViewModel.errorMessage.bind { error in
            if error != nil {
                print("Sell VC ERROR MESSAGE: ", error ?? "--")
            }
        }
    }
    
}

extension SellListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sellViewModel.data.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SellCell.cellIdentifier, for: indexPath) as! SellCell
        cell.config(data: sellViewModel.data.value[indexPath.row])
        return cell
    }
    
}
