//
//  SellCell.swift
//  TestDemo
//
//  Created by Arqam Butt on 01/08/2022.
//

import UIKit

class SellCell: UITableViewCell {

    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userPriceLbl: UILabel!
    @IBOutlet weak var userQuantityLbl: UILabel!
    
    static let cellIdentifier = "cell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "SellCell", bundle: nil)
    }
    
    func config(data: ItemToSell) {
        userNameLbl.text = "Name:\t\(data.name ?? "--")"
        userPriceLbl.text = "Price:\t\(data.price)"
        userQuantityLbl.text = "Qty:\t\(data.quantity)"
    }
    
}
