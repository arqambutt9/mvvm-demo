//
//  SellDataBaseReposatory.swift
//  TestDemo
//
//  Created by Arqam Butt on 01/08/2022.
//

import Foundation
import CoreData

struct SellDataBaseReposatory {
    
    private let psContext = PresistentStorage.shared
    
    func create(json: [[String:Any]]) {
        // Saving the data in core data
        json.forEach { obj in
            let sell = SellModel.init(dic: obj)
            let item = ItemToSell(context: PresistentStorage.shared.context)
            item.id =  Int16(sell.id ?? 0)
            item.price = Int64(sell.price ?? 0)
            item.name = sell.name
            item.quantity = Int16(sell.quantity ?? 0)
            item.type = Int16(sell.type ?? 0)
            psContext.saveContext()
        }
    }
    
    func getAll() -> [ItemToSell]? {
        let result = self.fetchManageObject(manageObject: ItemToSell.self)
        return result
    }
    
    func delete(all record: [ItemToSell]) -> [ItemToSell]? {
        for i in record {
            psContext.context.delete(i)
        }
        
        // Saving the data in core data
        psContext.saveContext()
        return self.getAll()
    }
    
    func fetchManageObject<T: NSManagedObject>(manageObject: T.Type) -> [T]? {
        do {
            guard let result = try
                    psContext.context.fetch(manageObject.fetchRequest()) as? [T] else {
                return nil
            }
            return result
        } catch let error {
            debugPrint(error.localizedDescription)
        }
        return nil
    }
    
}
