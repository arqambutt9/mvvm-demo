//
//  Se.swift
//  TestDemo
//
//  Created by Arqam Butt on 01/08/2022.
//
import ObjectMapper

class SellViewModel {
    
    typealias T = SellModel
    
    var data: Observable<[ItemToSell]> = Observable([])
    var errorMessage: Observable<String?> = Observable(nil)
    
    private let sellDataBaseManager: SellDataBaseManager?
    
    init(resource: SellDataBaseManager = SellDataBaseManager()) {
        self.sellDataBaseManager = resource
        self.fetchDataFromDatabase()
    }
    
    private func fetchDataFromDatabase() {
        data.value = Observable(sellDataBaseManager?.getAll() ?? []).value
    }
    
    func setDataFromDatabase() {
        let json = [
            ["id":1, "name":"One Plus", "price":129000, "quantity":1, "type":2],
            ["id":2, "name":"Samsung 21 Ultra", "price":320000, "quantity":1, "type":2],
            ["id":3, "name":"Oppe 541", "price":54000, "quantity":1, "type":2]
        ]
        sellDataBaseManager?.create(json: json)
        fetchDataFromDatabase()
    }
    
}
