//
//  Sell Model.swift
//  TestDemo
//
//  Created by Arqam Butt on 01/08/2022.
//

import ObjectMapper

internal final class SellModel: Mappable {
    var id: Int?
    var name: String?
    var price: Int?
    var quantity: Int?
    var type: Int?

    required init?(map: Map) {
        self.mapping(map: map)
        
    }

    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        id         <- map["id"]
        name       <- map["name"]
        price      <- map["price"]
        quantity   <- map["quantity"]
        type       <- map["type"]
    }
    
}
