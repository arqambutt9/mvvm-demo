//
//  AppProtocols.swift
//  TestDemo
//
//  Created by Arqam Butt on 31/07/2022.
//

import Foundation

protocol ObservableViewModelProtocol: AnyObject {
    associatedtype T
    func fetchDataFromApi(debugType debugStatus: DebugType)
    var data: Observable<[T]> { get  set } //1
    var errorMessage: Observable<String?> { get set }
}
