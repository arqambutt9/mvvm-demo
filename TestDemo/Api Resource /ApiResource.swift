//
//  Generic Resource.swift
//  TestDemo
//
//  Created by Arqam Butt on 01/08/2022.
//

import ObjectMapper

class ApiResource {
    private var network = NetworkLayer()
    private let sellDataBaseManager = SellDataBaseManager()
    
    func fetchApiData<T>(router: Router, model: T.Type, completion: @escaping (_ result: [T]?) -> Void, failure: @escaping Failure) where T : Mappable {
        network.request(route: router) { result in
            let res = Mapper<T>().mapArray(JSONString: result)
            completion(res)
        } failure: { error in
            failure(error)
        }

    }

}
