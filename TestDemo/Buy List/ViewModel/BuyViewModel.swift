//
//  BuyViewModel.swift
//  TestDemo
//
//  Created by Arqam Butt on 31/07/2022.
//

class BuyViewModel: ObservableViewModelProtocol {
    
    typealias T = BuyModel
    
    var data: Observable<[BuyModel]> = Observable([])
    var errorMessage: Observable<String?> = Observable(nil)
    
    private var resource: ApiResource?
    
    init(resource: ApiResource = ApiResource()) {
        self.resource = resource
    }
    
    func fetchDataFromApi(debugType debugStatus: DebugType) {
        resource?.fetchApiData(router: .type(.noParams(.buy)), model: BuyModel.self, completion: { [weak self] (result) in
            if debugStatus == .enable {
                debugPrint("Code: \(result!)")
                debugPrint("Data: \(String(describing: result!))")
            }
            if let res = result {
                self?.data.value = Observable(res).value
            }else {
                self?.setError("Multiple Errors To Show")
            }
        }, failure: { [weak self] (error) in
            self?.setError(error)
        })
    }
    
    private func setError(_ message: String) {
        self.errorMessage.value = Observable(message).value
    }
    
}

