//
//  BuyListVC.swift
//  Test 1
//
//  Created by Arqam Butt on 31/07/2022.
//

import UIKit

class BuyListVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var buyViewModel = BuyViewModel()
    
    deinit {
        print("Buy De init")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(BuyCell.nib(), forCellReuseIdentifier: BuyCell.cellIdentifier)
        buyViewModel.fetchDataFromApi(debugType: .disable)
        bindData()
    }

    private func bindData() {
        buyViewModel.data.bind { [weak self] _ in
            self?.tableView.reloadData()
        }
        
        buyViewModel.errorMessage.bind { error in
            if error != nil {
                print("BUY VC ERROR MESSAGE: ", error ?? "--")
            }
        }
    }

}

extension BuyListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return buyViewModel.data.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BuyCell.cellIdentifier, for: indexPath) as! BuyCell
        cell.config(data: buyViewModel.data.value[indexPath.row])
        return cell
    }
    
}
