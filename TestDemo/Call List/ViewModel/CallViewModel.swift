//
//  CallViewModel.swift
//  TestDemo
//
//  Created by Arqam Butt on 31/07/2022.
//

class CallViewModel: ObservableViewModelProtocol {
    
    typealias T = CallModel
    
    var data: Observable<[CallModel]> = Observable([])
    var errorMessage: Observable<String?> = Observable(nil)
    
    private var resource: ApiResource?
    
    init(resource: ApiResource = ApiResource()) {
        self.resource = resource
    }
    
    func fetchDataFromApi(debugType debugStatus: DebugType) {
        
        resource?.fetchApiData(router: .type(.noParams(.call)), model: CallModel.self, completion: { [weak self] (result) in
            if debugStatus == .enable {
                debugPrint("Code: \(result!)")
                debugPrint("Data: \(String(describing: result!))")
            }
            if let res = result {
                self?.data.value = Observable(res).value
            }else {
                self?.setError("Multiple Errors To Show")
            }
        }, failure: { [weak self] (error) in
            self?.setError(error)
        })

    }
    
    private func setError(_ message: String) {
        self.errorMessage.value = Observable(message).value
    }
    
}

//class CallViewModel {
//
//    weak var delegate: ResponseDelegate?
//    private let resource = CallResource()
//
//    var callModel = [CallModel]() {
//        didSet {
//            delegate?.didReceiveResponse()
//        }
//    }
//
//    func getCallData(debugType debugStatus: DebugType) {
//        resource.callApi { [weak self] (result) in
//            if debugStatus == .enable {
//                debugPrint("Code: \(result!)")
//                debugPrint("Data: \(String(describing: result!))")
//            }
//            if let res = result {
//                self?.callModel = res
//            }else {
//                self?.delegate?.didReceiveError(with: "Multiple Errors To Show")
//            }
//        } failure: { [weak self] (error) in
//            self?.delegate?.didReceiveError(with: error)
//        }
//    }
//
//}
