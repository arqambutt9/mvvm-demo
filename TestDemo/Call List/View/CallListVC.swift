//
//  CallListVC.swift
//  Test 1
//
//  Created by Arqam Butt on 31/07/2022.
//

import UIKit
import Alamofire

class CallListVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var callViewModel = CallViewModel()
    
    deinit {
        print("Call De init")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(CallCell.nib(), forCellReuseIdentifier: CallCell.cellIdentifier)
        callViewModel.fetchDataFromApi(debugType: .disable)
        bindData()
    }

    private func bindData() {
        callViewModel.data.bind { [weak self] _ in
            self?.tableView.reloadData()
        }
        
        callViewModel.errorMessage.bind { error in
            if error != nil {
                print("CALL VC ERROR MESSAGE: ", error ?? "--")
            }
        }
    }
    
}

extension CallListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return callViewModel.data.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CallCell.cellIdentifier, for: indexPath) as! CallCell
        cell.config(data: callViewModel.data.value[indexPath.row])
        return cell
    }
    
}

