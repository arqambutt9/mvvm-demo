//
//  CallCell.swift
//  TestDemo
//
//  Created by Arqam Butt on 31/07/2022.
//

import UIKit

class CallCell: UITableViewCell {

    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userNumberLbl: UILabel!
    
    static let cellIdentifier = "cell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static func nib() -> UINib {
        return UINib(nibName: "CallCell", bundle: nil)
    }
    
    func config(data: CallModel) {
        userNameLbl.text = "Name:\t\(data.name ?? "--")"
        userNumberLbl.text = "Number:\t\(data.number ?? "--")"
    }
    
}
