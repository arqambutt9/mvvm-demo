//
//  Call Model;.swift
//  TestDemo
//
//  Created by Arqam Butt on 31/07/2022.
//

import Foundation
import ObjectMapper

internal final class CallModel: Mappable {
    var id: Int?
    var name: String?
    var number: String?
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        id         <- map["id"]
        name       <- map["name"]
        number     <- map["number"]
    }
}
