//
//  ViewController.swift
//  Test 1
//
//  Created by Arqam Butt on 31/07/2022.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    
    
    @IBAction func callAction(_ sender: Any) {
        performSegue(withIdentifier: "callSegue", sender: nil)
    }
    
    @IBAction func buyAction(_ sender: Any) {
        performSegue(withIdentifier: "buySegue", sender: nil)
    }
    
    @IBAction func sellAction(_ sender: Any) {
        performSegue(withIdentifier: "sellSegue", sender: nil)
    }
    
}

