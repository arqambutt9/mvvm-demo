//
//  PresistentStorage.swift
//  DopeChatDemo
//
//  Created by Arqam Butt on 13/02/2022.
//

import Foundation
import CoreData

final class PresistentStorage {
    
    static var shared = PresistentStorage()
    
    private init() {}
    
    // To fetch and save object graph in presistent store
    lazy var context = persistentContainer.viewContext
    
    
    // Presistent container is responsible for creating and managing the core data stack.
    lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "TestDemo")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            container.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
        
    }()

    
    // Core Data Saving support
    func saveContext () {
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
    
    
}
