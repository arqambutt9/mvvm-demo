//
//  Enum.swift
//  TestDemo
//
//  Created by Arqam Butt on 31/07/2022.
//

import Foundation

enum DebugType {
    case disable
    case enable
}
