//
//  Utilities.swift
//  TestDemo
//
//  Created by Arqam Butt on 01/08/2022.
//

import Foundation
import CoreData

class Utilities {
    
    static var shared = Utilities()
    
    private init() {}
    private let persistentContainer = PresistentStorage.shared.persistentContainer
    
    func clearStorage(forEntity entity: String) {
        let isInMemoryStore = persistentContainer.persistentStoreDescriptions.reduce(false) {
                return $0 ? true : $1.type == NSInMemoryStoreType
            }

            let managedObjectContext = persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
            // NSBatchDeleteRequest is not supported for in-memory stores
            if isInMemoryStore {
                do {
                    let entities = try managedObjectContext.fetch(fetchRequest)
                    for entity in entities {
                        managedObjectContext.delete(entity as! NSManagedObject)
                    }
                } catch let error as NSError {
                    print(error)
                }
            } else {
                let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
                do {
                    try managedObjectContext.execute(batchDeleteRequest)
                } catch let error as NSError {
                    print(error)
                }
            }
    }
    
}
