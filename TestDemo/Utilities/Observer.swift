//
//  Observer.swift
//  TestDemo
//
//  Created by Arqam Butt on 01/08/2022.
//

import Foundation

final class Observable<T> {

    typealias Listener = (T) -> Void
    
    private var listener: Listener?
    
    var value: T {
        didSet {
            listener?(value)
        }
    }

    init(_ value: T) {
        self.value = value
    }

    func bind(_ closure: Listener?) {
        self.listener = closure
        closure?(value)
    }
    
}
