//
//  TypeAlias.swift
//  TestDemo
//
//  Created by Arqam Butt on 31/07/2022.
//

import Foundation

typealias Failure = (_ error: String) -> Void
typealias Success = (_ result: String) -> Void
