//
//  NetworkLayer.swift
//  TestDemo
//
//  Created by Arqam Butt on 31/07/2022.
//

import Foundation
import Alamofire

class NetworkLayer: NSObject {
    deinit {
        print("Network Layer De-init")
    }
    
    private func alamofireRequest(route: Router, success: @escaping Success, failure: @escaping Failure) {
        AF.request(route).validate().responseString { response in
            switch response.result {
            case .success(let value):
                success(value)
            case .failure(let error):
                failure(error.localizedDescription)
            }
        }
    }
    
    func request(route: Router, success: @escaping Success, failure: @escaping Failure) {
        alamofireRequest(route: route, success: success, failure: failure)
    }
}
