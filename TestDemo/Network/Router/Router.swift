//
//  Router.swift
//  Test 1
//
//  Created by Arqam Butt on 31/07/2022.
//

import Foundation
import Alamofire

enum Router: URLRequestConvertible {
    case type(RouterType)

    private var baseURL: URL {
        return URL(string: Constants.baseURL)!
    }

    private var method: HTTPMethod {
        switch self {
        case .type(.noParams): return .get
        case .type(.params(_, let _type, _)):
            switch _type {
            case .post:
                return .post
            case .getWithParams:
                return .get
            }
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = baseURL.appendingPathComponent(path)

        var request = URLRequest(url: url)
        request.method = method
        switch self {
        case let .type(_subType):
            switch _subType {
            case .params(let parameters, let requestType, _):
                debugPrint("Parameters: \(parameters)")
                switch requestType {
                case .post:
                    request = try JSONParameterEncoder().encode(parameters, into: request)
                    break
                case .getWithParams:
                    request = try URLEncodedFormParameterEncoder().encode(parameters, into: request)
                    break
                }
            case .noParams:
                debugPrint("URL: \(request.url!)")
                return request
            }
        }
        debugPrint("URL: \(request.url!)")
        return request
    }
}

extension Router {
    internal enum RouterType {
        case params([String:String], RequestType, ApiType)
        case noParams(ApiType)
    }

    internal enum RequestType {
        case post
        case getWithParams
    }
    
    internal enum ApiType {
        case call
        case buy
    }
}

extension Router {
    private var path: String {
        switch self {
        case .type(let routerType):
            switch routerType {
            case .params(_, _, let apiType):
                switch apiType {
                default: break
                }
            case .noParams(let apiType):
                switch apiType {
                case .call:
                    return "call"
                case .buy:
                    return "buy"
                }
            }
        }
        return "Somethings Off"
    }
}
